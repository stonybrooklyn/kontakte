package com.shaneheartman.kontakte;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by stonybrooklyn on 7/26/2015.
 */
public class JsHandler
{
    public static final String EXTRA_CONTACT_POS = "com.shaneheartman.android.kontakte.contact_pos";
    Activity activity;
    String TAG = "JsHandler";
    WebView webView;

    private ArrayList<Contact> mContacts;


    public JsHandler(Activity _contxt,WebView _webView) {
        activity = _contxt;
        webView = _webView;
    }

    /**
     * This function handles call from JS
     */
    @JavascriptInterface
    public void jsFnCall(String jsString) {
        returnToList();
    }

    /**
     * This function handles call from Android-Java
     */
    @JavascriptInterface
    public void javaFnCall(String jsString) {
        Log.d(TAG, "Random message js function called");

        mContacts = ContactStore.get(activity.getApplication()).getContacts();
        int howMany = mContacts.size();
        int position = randInt(0, howMany);
        Contact c = mContacts.get(position);
        Intent i = new Intent(activity.getApplicationContext(), ContactView.class);
        i.putExtra(MainActivity.EXTRA_CONTACT_POS, position);
        activity.startActivity(i);
    }


    public void returnToList () {
        Intent i = new Intent(activity, MainActivity.class);
        activity.startActivity(i);

    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
