package com.shaneheartman.kontakte;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;


public class MainActivity extends ListActivity {

    protected ImageButton mAddContactButton;
    private ArrayList<Contact> mContacts;
    public static final String EXTRA_CONTACT_ID = "com.shaneheartman.android.kontakte.contact_id";
    public static final String EXTRA_CONTACT_POS = "com.shaneheartman.android.kontakte.contact_pos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ContactStore.get(this).sort();
        mContacts = ContactStore.get(this).getContacts();

        String[] contacts = {"John", "Bob", "Sue", "Jane", "Sam", "Allen", "Josh", "Betty", "Jane", "Lisa"};
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contacts);
        //setListAdapter(adapter);

        ContactAdapter adapter = new ContactAdapter(this, mContacts);

        setListAdapter(adapter);
        //mAddContactButton.setImageResource(android.R.drawable.ic_menu_invite);
        mAddContactButton = (ImageButton)findViewById(R.id.addContactButton);

        mAddContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onListItemClick (ListView l, View v, int position, long id) {
        Contact c = (Contact) (getListAdapter()).getItem(position);
        Intent i = new Intent(this, ContactView.class);
        //UUID contactID = c.getmId();
        i.putExtra(MainActivity.EXTRA_CONTACT_POS, position);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
