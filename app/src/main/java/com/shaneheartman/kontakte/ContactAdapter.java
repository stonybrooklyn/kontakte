package com.shaneheartman.kontakte;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by stonybrooklyn on 7/26/2015.
 */
public class ContactAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Contact> mContacts;

    public ContactAdapter(Context context, ArrayList<Contact> contacts) {
        mContext = context;
        mContacts = contacts;
    }
    @Override
    public int getCount() {
        return mContacts.size();
    }

    @Override
    public Object getItem(int position) {
        return mContacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0; //do not need to implement
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.contact_list_item, null);
            holder = new ViewHolder();
            holder.contactImageView = (ImageView) convertView.findViewById(R.id.list_contact_image);
            holder.firstnameLabel = (TextView) convertView.findViewById(R.id.list_firstname);
            holder.lastnameLabel = (TextView) convertView.findViewById(R.id.list_lastname);

            convertView.setTag(holder);
        }
        //else {

        holder = (ViewHolder) convertView.getTag();

        Contact contact = mContacts.get(position);
            //ImageView contactImagge = new ImageView(this, MainActivity.class);
        ImageView contactImageView = (ImageView) convertView.findViewById(R.id.list_contact_image);
        //TextView firstnameLabel = (TextView) convertView.findViewById(R.id.list_firstname);
        //TextView lastnameLabel = (TextView) convertView.findViewById(R.id.list_lastname);

        //contactImageView.setImageResource(Contact.getIconId());
        //firstnameLabel.setText(contact.getmName());
        //lastnameLabel.setText(contact.getmLast());
        holder.contactImageView.setImageResource(contact.getIconId());
        holder.firstnameLabel.setText(contact.getmName());
        holder.lastnameLabel.setText(contact.getmLast());
        //}

        return convertView;
    }

    private static class ViewHolder {
        ImageView contactImageView;
        TextView firstnameLabel;
        TextView lastnameLabel;
    }
}
