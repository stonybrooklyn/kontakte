package com.shaneheartman.kontakte;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.http.SslError;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.util.ArrayList;


public class ContactView extends Activity implements View.OnClickListener {

    public static final String EXTRA_CONTACT_POS = "com.shaneheartman.android.kontakte.contact_pos";

    private WebView initialWebView;
    private WebView webView;


    private JsHandler _jsHandler;
    private Contact thisContact;
    private String data;
    private int position;
    private ArrayList<Contact> mContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_view);
        initWebView();
    }

    /**
     * Function initializes  webview & does the necessary settings for webview
     */
    private void initWebView(){

        initialWebView = (WebView) findViewById(R.id.contactView);
        webView = (WebView)findViewById(R.id.contact_toolView);
        //Tell the WebView to enable javascript execution.
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.parseColor("#ffffff"));

        //Set whether the DOM storage API is enabled.
        webView.getSettings().setDomStorageEnabled(true);

        //setBuiltInZoomControls = false, removes +/- controls on screen
        webView.getSettings().setBuiltInZoomControls(false);

        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);

        webView.getSettings().setAppCacheMaxSize(1024 * 8);
        webView.getSettings().setAppCacheEnabled(true);

        _jsHandler = new JsHandler(this, webView);
        webView.addJavascriptInterface(_jsHandler, "JsHandler");

        webView.getSettings().setUseWideViewPort(false);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);


            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }

            @Override
            public void onReceivedSslError(WebView view,
                                           SslErrorHandler handler, SslError error) {
                // TODO Auto-generated method stub
                super.onReceivedSslError(view, handler, error);


            }
        });
        //this webview appears at the bottom of screen and offers js buttons to close view or get random contact
        // these settings speed up page load into the webview
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.requestFocus(View.FOCUS_DOWN);
        // load the main.html file that kept in assets folder
        webView.loadUrl("file:///android_asset/contact.html");

        //this webview appears at the top and displays the contact info for the person selected in listview

        //retrieve information passed from listview in main activity
        position = getIntent().getIntExtra(EXTRA_CONTACT_POS, -1);
        mContacts = ContactStore.get(this).getContacts();
        Contact passedContact = mContacts.get(position);

        //assign name, phone# & email
        String name = passedContact.getmName() + " " + passedContact.getmLast();
        String email = passedContact.getmEmail();
        String phone = passedContact.getmPhone();

        //format html data for presentation in webview
        String htmlHeader = "<html><head><title>Contact Info</title></head><body>";
        String htmlContent = "<h1>" + name + "</h1>" + "<h2><font color=\"blue\">" + email + "</font></h2>" + "<h2><font color=\"blue\">" + phone + "</font></h2>";
        String htmlFooter = "</body></html>";


        data = htmlHeader + htmlContent + htmlFooter;
        initialWebView.getSettings().setJavaScriptEnabled(true);
        initialWebView.loadDataWithBaseURL("", data, "text/html", "UTF-8", "");

    }

    @Override
    public void onClick(View v) {

    }
}