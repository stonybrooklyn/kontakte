package com.shaneheartman.kontakte;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class AddContactActivity extends AppCompatActivity {

    protected EditText mFirstname;
    protected EditText mLastname;
    protected EditText mPhonenumber;
    protected EditText mEmail;
    protected Button mDoneButton;
    protected Button mCancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        mFirstname = (EditText)findViewById(R.id.txtFirstname);
        mLastname = (EditText)findViewById(R.id.txtLastname);
        mPhonenumber = (EditText)findViewById(R.id.txtPhone);
        mEmail = (EditText)findViewById(R.id.txtEmail);
        mDoneButton = (Button)findViewById(R.id.btnDone);
        mCancelButton = (Button)findViewById(R.id.btnCancel);
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstname = mFirstname.getText().toString();
                String lastname = mLastname.getText().toString();
                String phone = mPhonenumber.getText().toString();
                String email = mEmail.getText().toString();

                firstname = firstname.trim();
                lastname = lastname.trim();
                phone = phone.trim();
                email = email.trim();

                if (firstname.isEmpty() && lastname.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddContactActivity.this);
                    builder.setMessage(R.string.addcontact_error_message)
                            .setTitle(R.string.addcontact_error_title)
                            .setPositiveButton(android.R.string.ok, null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    //save contact
                    Contact contact = new Contact();
                    contact.setmName(firstname);
                    contact.setmLast(lastname);
                    contact.setmEmail(email);
                    contact.setmPhone(phone);
                    ContactStore.get(AddContactActivity.this).addContact(contact);
                    Intent i = new Intent(AddContactActivity.this, MainActivity.class);
                    startActivity(i);
                }
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddContactActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
