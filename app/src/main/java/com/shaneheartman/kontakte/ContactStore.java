package com.shaneheartman.kontakte;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;


/**
 * Created by stonybrooklyn on 5/31/2015.
 */




/**
 * Created by stonybrooklyn on 7/22/2015.
 */
public class ContactStore {

    private static ContactStore sContactStore;
    private Context mAppContext;
    private ArrayList<Contact> mContacts;

    public ArrayList<Contact> getContacts() {
        return mContacts;
    }

    public Contact getContact(UUID id) {
        for (Contact p: mContacts) {
            if (p.getmId().equals(id))
                return p;
        }
        return  null;
    }

    public void addContact(Contact c) {
        mContacts.add(c);
    }

    private ContactStore(Context appContext) {
        mAppContext = appContext;
        mContacts = new ArrayList<Contact>();
    }

    public static ContactStore get(Context c) {
        if (sContactStore == null) {
            sContactStore = new ContactStore(c.getApplicationContext());
        }
        return sContactStore;
    }

    public void sort () {
        ArrayList<Contact> newContacts = new ArrayList<Contact>();
        newContacts.addAll(mContacts);
        Collections.sort(newContacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c1.toString().compareTo(c2.toString());
            }
        });
        mContacts = newContacts;

    }

}
