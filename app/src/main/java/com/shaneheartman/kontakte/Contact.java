package com.shaneheartman.kontakte;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by stonybrooklyn on 7/22/2015.
 */
public class Contact {

    private UUID mId;
    private String mName;
    private String mLast;
    private String mEmail;
    private String mPhone;


    public Contact () {

        mId = UUID.randomUUID();
    }

    public String getmName() {
        return mName;
    }

    public UUID getmId() {
        return mId;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmLast() {
        return mLast;
    }

    public void setmLast(String mLast) {
        this.mLast = mLast;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public static int getIconId() {

        return R.drawable.contact_image_placeholder;
    }

    @Override
    public String toString() {

        return mName;
    }













}
